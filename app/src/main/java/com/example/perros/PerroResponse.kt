package com.example.perros

import com.google.gson.annotations.SerializedName
data class PerroResponse (
    @SerializedName("results") var results: ArrayList<Perro>
)