package com.example.perros

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface PerroAPI {
    @Headers("Accept: application/json")
    // Método para obtener todos los perros
    @GET("perro")
    fun getPerro(): Call<PerroResponse>
    // Método para obtener un perro por su ID
    @GET("perro/{id}")
    fun getPerro(@Path("id") id: Int): Call<Perro>
}
