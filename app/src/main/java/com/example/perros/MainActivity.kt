package com.example.perros

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.perros.ui.theme.PerrosTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : ComponentActivity() {
    private lateinit var retrofit : Retrofit
    private var texto: String = "Pruebas"
    override fun onCreate(savedInstanceState: Bundle?) {
        retrofit = retrofit2.Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/breeds/image/random")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        texto = obtenerDatos(retrofit)
        super.onCreate(savedInstanceState)
        setContent {
            PerrosTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {

                }
            }
        }
    }

    private fun obtenerDatos(retrofit : Retrofit) : String {
        var texto = "";
        CoroutineScope(Dispatchers.IO).launch {
            val call =
                retrofit.create(PerroAPI::class.java).getPerro().execute()
            val perros = call.body()
            if (call.isSuccessful) {
                texto = perros?.results?.get(1)?.getNombre().toString()
            } else {
                texto = "Ha habido un error"
            }
        }
        Thread.sleep(1000)
        return texto;
    }
}


@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    PerrosTheme {
        Greeting("Android")
    }
}